﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskCheck : MonoBehaviour
{
    public GameObject Tag;
    public GameObject Picture;
    public static bool CollecMask;
    // Start is called before the first frame update
    void Start()
    {
        CollecMask = false;
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Tag.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)&&JelCheck.CollecJel == false)
            {  
                Debug.Log("CollecMask"); 
                CollecMask = true;
                Picture.gameObject.SetActive(true);
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Tag.gameObject.SetActive(false);
        }
    }
}
