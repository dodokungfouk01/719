﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JelCheck : MonoBehaviour
{
    public GameObject Tag;
    public GameObject Picture;
    public static bool CollecJel;
    // Start is called before the first frame update
    void Start()
    {
        CollecJel = false;
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Tag.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)&&MaskCheck.CollecMask == false)
            {   
                Debug.Log("CollecJel");
                Picture.gameObject.SetActive(true);
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Tag.gameObject.SetActive(false);
        }
    }
}
