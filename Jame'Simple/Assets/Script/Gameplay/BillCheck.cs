﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillCheck : MonoBehaviour
{
    public GameObject cube;
    public static bool Bill;
    // Start is called before the first frame update
    void Start()
    {
        Bill = false;
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("B");
            Bill = true;
            cube.gameObject.SetActive(true);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Bill = false;
            cube.gameObject.SetActive(false);
        }
    }
}
