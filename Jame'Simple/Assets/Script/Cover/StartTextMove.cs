﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTextMove : MonoBehaviour
{
    public float speed = 1.0f; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameInstance.Instance.logofin == true){
            if(transform.position.y < 30 ){
                transform.position  += transform.up * speed ;}
            else {GameInstance.Instance.textfin = true;}
        }
    }
}
