﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartSceneScript : MonoBehaviour
{

    [SerializeField] Button _startButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void StartClick(Button button){
        if(GameInstance.Instance.logofin == true && GameInstance.Instance.textfin == true){
            //SceneManager.UnloadScene("Cover");
            SceneManager.LoadScene("MainMenu");
        }
    }
}
