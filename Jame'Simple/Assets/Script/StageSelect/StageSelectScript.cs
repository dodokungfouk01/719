﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class StageSelectScript : MonoBehaviour
{
    [SerializeField] Button _stage1Button;
    [SerializeField] Button _backButton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackButtonClick(Button button)
    {
        SceneManager.UnloadScene("StageSelect");
        SceneManager.LoadScene("MainMenu");
    }

    public void Stage1Click(Button button)
    {
        SceneManager.UnloadScene("StageSelect");
        SceneManager.LoadScene("MainMenu");
    }
}
