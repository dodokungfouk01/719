﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameMove : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 3.0f;  
    void Start()
    {
        GameInstance.Instance.startBtnfin = false;
    }

    // Update is called once per frame
    void Update()
    {   
        if(transform.position.x > 800 ){
            transform.position  -= transform.right * speed;}
        else {GameInstance.Instance.startBtnfin = true;}
        }
}
