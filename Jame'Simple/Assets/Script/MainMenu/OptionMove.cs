﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionMove : MonoBehaviour
{
   public float speed = 3.0f; 
    // Start is called before the first frame update
    void Start()
    {
        GameInstance.Instance.optionsBtnfin = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameInstance.Instance.startBtnfin == true){
            if(transform.position.x > 800 ){
                transform.position  -= transform.right * speed;}
            else {GameInstance.Instance.optionsBtnfin = true;}
        }
    }
}
