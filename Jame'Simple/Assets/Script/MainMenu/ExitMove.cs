﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitMove : MonoBehaviour
{
    // Start is called before the first frame update
   public float speed = 3.0f; 
    // Start is called before the first frame update
    void Start()
    {
        GameInstance.Instance.exitBtnfin = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameInstance.Instance.optionsBtnfin == true){
            if(transform.position.x > 800 ){
                transform.position  -= transform.right * speed;}
            else {GameInstance.Instance.exitBtnfin = true;}
        }
    }
}
