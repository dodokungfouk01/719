﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] Button _StartGameButton;
    [SerializeField] Button _OptionsButton;
    [SerializeField] Button _ExitButton;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGameButtonClick(Button button)
    {   if(GameInstance.Instance.exitBtnfin == true){

            SceneManager.UnloadScene("MainMenu");
            SceneManager.LoadScene("StageSelect");
            
        }
    }
    
    public void OptionsButtonClick(Button button)
    {   
        if(GameInstance.Instance.exitBtnfin == true){
            if (GameInstance.Instance.IsOptionMenuActive == false)
            {
                SceneManager.LoadScene("Options", LoadSceneMode.Additive);
                GameInstance.Instance.IsOptionMenuActive = true;
            }
        }
    }
    
    public void ExitButtonClick(Button button)
    {
        if(GameInstance.Instance.exitBtnfin == true){
            Application.Quit();
        }
    }
    
}
